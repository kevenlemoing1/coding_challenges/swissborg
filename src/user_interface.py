from math import exp

from src.data_handler import ForexGraph


def display_arbitrages(graph: ForexGraph, paths: list):
    for path in paths:
        if path is None:
            print("No arbitrage found :(")
        else:
            starting_sum = 100
            for i, value in enumerate(path):
                if i + 1 < len(path):
                    start = path[i]
                    end = path[i + 1]
                    rate = exp(-graph.conn[start][end])
                    starting_sum *= rate
                    print('{}) {} to {} at {}'.format(i, start, end, rate))
            print('The total rate of this arbitrage is: {}'.format(starting_sum/100))
        print('\n')