from typing import Tuple, Optional

from src.data_handler import ForexGraph


def init_bellman_ford(graph: dict, source: str) -> Tuple[dict, dict]:
    """ Initialisation steps for being able to run Bellman - Ford on a given graph """
    curr_to = {}
    curr_from = {}
    for node in graph:
        curr_to[node] = float('Inf')
        curr_from[node] = None
    curr_to[source] = 0
    return curr_to, curr_from


def bellman_ford(graph: dict, source: str) -> Optional[list]:
    """ Typical Bellman - Ford iterations with an added value of recording negative cycles paths"""
    curr_dest, curr_pred = init_bellman_ford(graph, source)
    # Bellman ford iterations
    for i in range(len(graph) - 1):
        for curr_from in graph:
            for curr_to in graph[curr_from]:
                if curr_dest[curr_to] > curr_dest[curr_from] + graph[curr_from][curr_to]:
                    # Record shortest distance
                    curr_dest[curr_to] = curr_dest[curr_from] + graph[curr_from][curr_to]
                    curr_pred[curr_to] = curr_from

    # Identify negative-weight cycles
    for curr_from in graph:
        for curr_to in graph[curr_from]:
            # If this condition is matched, we have negative cycles
            if curr_dest[curr_to] < curr_dest[curr_from] + graph[curr_from][curr_to]:
                # Record negative cycles path / arbitrages
                arbitrage_path = [source]
                next_node = source
                while True:
                    next_node = curr_pred[next_node]
                    if next_node not in arbitrage_path:
                        arbitrage_path.append(next_node)
                    else:
                        arbitrage_path.append(next_node)
                        arbitrage_path = arbitrage_path[arbitrage_path.index(next_node):]
                        return arbitrage_path
    print('No arbitrage opportunity !')
    return None


def get_arbitrages(graph: ForexGraph) -> list:
    """ Apply Bellman - Ford algorithm on a each currency / node of the graph"""
    paths = []
    for src in graph.conn:
        # Running Bellman-Ford algorithm on each currency
        path = bellman_ford(graph.conn, src)
        if path not in paths and not None:
            paths.append(path)
    return paths
