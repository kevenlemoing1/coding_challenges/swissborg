import requests
from math import log

from src.const import url_market_prices


def fetch_market() -> dict:
    """ Get rates exchange from priceonomics' api """
    url_content = requests.get(url_market_prices)
    return url_content.json()


class ForexGraph:

    def __init__(self, market_data: dict):
        self.conn = dict()
        self.parse_market_data(market_data)
        self.nodes = [self.conn.keys()]
        self.nb_nodes = len(self.nodes)
        self.nb_edges = len(market_data)

    def parse_market_data(self, payload: dict):
        """ Graph implementation where each edge is specified as a sub-dictionary """
        for key in payload.keys():
            rate = -log(float(payload[key]))
            curr_from = key.split('_')[0]
            curr_to = key.split('_')[1]
            if curr_from != curr_to:
                if curr_from not in self.conn:
                    self.conn[curr_from] = {}
                self.conn[curr_from][curr_to] = rate
