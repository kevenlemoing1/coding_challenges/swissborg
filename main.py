from src.data_handler import fetch_market, ForexGraph
from src.graph_handler import get_arbitrages
from src.user_interface import display_arbitrages


def main():
    forex_data = fetch_market()
    forex_graph = ForexGraph(forex_data)
    arbitrages = get_arbitrages(forex_graph)
    display_arbitrages(forex_graph, arbitrages)


if __name__ == "__main__":
    main()
